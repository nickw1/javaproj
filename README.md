AWT-FREE PROJ4JAVA
==================

This is a modified version of Jerry Huxtable's Proj4Java (Java Proj.4 port;
see https://trac.osgeo.org/proj4j/) with all references to the AWT removed, to 
allow inclusion in Android projects.

History of no-AWT version:

* June/July 2011: original javaproj-1.0.6-noawt
* 6/1/18: revision 2, removed remaining AWT references from projection files
* 20/2/18: revision 3, corrected pom.xml to include "nad" directory

Modifications Nick Whitelegg
