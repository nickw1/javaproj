package com.jhlabs.map;

public abstract class Rectangle2D {

	public abstract double getX();
	public abstract double getY();
	public abstract double getWidth();
	public abstract double getHeight();
	public abstract void add(double px,double py);
	
	public static class Double extends Rectangle2D
	{
		double x,y,w,h;
		
		public Double(double xIn,double yIn,double wIn,double hIn)
		{
			x=xIn;
			y=yIn;
			w=wIn;
			h=hIn;
		}
		public double getX()
		{
			return x;
		}
		public double getY()
		{
			return y;
		}
		public double getWidth()
		{
			return w;
		}
		public double getHeight()
		{
			return h;
		}
		
		public void add(double px,double py)
		{
			if(px<x)
				x=px;
			else if(px>x+w)
				w=px-x;
			if(py<y)
				y=py;
			else if(py>y+h)
				h=py-y;
		}
	}
}
