// Replacement Point2D to remove AWT dependency

package com.jhlabs.map;

public class Point2D
{
	public static class Double
	{
	
		public double x,y;
		public Double(){x=y=0.0; }
		public Double(double xIn,double yIn) { x=xIn;y=yIn; }
		public String toString() { return "com.jhlabs.map.Point2D.Double: x="+x+" y=" + y; }
	}


}

